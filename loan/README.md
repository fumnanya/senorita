# loan

tiny modelling assignment.

### to run:

1. install [`rye`](https://rye-up.com/guide/installation/).
2. clone the project: `git clone https://codeberg.org/fumnanya/senorita`.
3. change into this directory: `cd loan`
4. install the dependencies: `rye sync`.
5. run the app: `rye run loan`.

### images:

| |
|:-------:| 
|![](images/admin_dash.png)|
| _the admin dashboard_ |
|![](images/user_dash.png)|
| _the user dashboard_ |
|![](images/user_register.png)|
| _the user registration page_ |
|![](images/user_register.png)|
| _the admin dashboard_ |
