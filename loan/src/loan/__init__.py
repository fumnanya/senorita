import pytermgui as ptg
import loan.api as api

import tabulate
import plotext as plt
import numpy.polynomial as py


PALETTE_LIGHT = "#FCBA03"
PALETTE_MID = "#8C6701"
PALETTE_DARK = "#4D4940"
PALETTE_DARKER = "#242321"


def _create_aliases() -> None:
    """Creates all the TIM aliases used by the application.

    Aliases should generally follow the following format:

        namespace.item

    For example, the title color of an app named "myapp" could be something like:

        myapp.title
    """

    ptg.tim.alias("app.text", "#cfc7b0")

    ptg.tim.alias("app.header", f"bold @{PALETTE_MID} #d9d2bd")
    ptg.tim.alias("app.header.fill", f"@{PALETTE_LIGHT}")

    ptg.tim.alias("app.title", f"bold {PALETTE_LIGHT}")
    ptg.tim.alias("app.button.label", f"bold @{PALETTE_DARK} app.text")
    ptg.tim.alias("app.button.highlight", "inverse app.button.label")

    ptg.tim.alias("app.footer", f"@{PALETTE_DARKER}")


def _configure_widgets() -> None:
    """Defines all the global widget configurations.

    Some example lines you could use here:

        ptg.boxes.DOUBLE.set_chars_of(ptg.Window)
        ptg.Splitter.set_char("separator", " ")
        ptg.Button.styles.label = "myapp.button.label"
        ptg.Container.styles.border__corner = "myapp.border"
    """

    ptg.boxes.DOUBLE.set_chars_of(ptg.Window)
    ptg.boxes.ROUNDED.set_chars_of(ptg.Container)

    ptg.Button.styles.label = "app.button.label"
    ptg.Button.styles.highlight = "app.button.highlight"

    ptg.Slider.styles.filled__cursor = PALETTE_MID
    ptg.Slider.styles.filled_selected = PALETTE_LIGHT

    ptg.Label.styles.value = "app.text"

    ptg.Window.styles.border__corner = "#C2B280"
    ptg.Container.styles.border__corner = PALETTE_DARK

    ptg.Splitter.set_char("separator", "")


def _define_layout() -> ptg.Layout:
    """Defines the application layout.

    Layouts work based on "slots" within them. Each slot can be given dimensions for
    both width and height. Integer values are interpreted to mean a static width, float
    values will be used to "scale" the relevant terminal dimension, and giving nothing
    will allow PTG to calculate the corrent dimension.
    """

    layout = ptg.Layout()

    # A header slot with a height of 1
    layout.add_slot("Header", height=1)
    layout.add_break()

    # A body slot that will fill the entire width, and the height is remaining
    layout.add_slot("Body")

    # A slot in the same row as body, using the full non-occupied height and
    # 20% of the terminal's height.
    # layout.add_slot("Body right", width=0.2)

    layout.add_break()

    # A footer with a static height of 1
    layout.add_slot("Footer", height=1)

    return layout


def _ask_admin(manager: ptg.WindowManager) -> None:
    """Asks what kind of user you are."""

    modal = ptg.Window(
        "[app.title]What are you?",
        "",
        ptg.Container(
            ptg.Splitter(
                ptg.Button("Administrator", lambda _: _show_admin_dash(manager)),
                ptg.Button("User", lambda _: _login_user(manager)),
            ),
            static_width=50,
        ),
    ).center()

    manager.add(modal, assign="body")


def _show_admin_dash(manager: ptg.WindowManager) -> None:
    """Show the main admin. dashboard."""

    total_pay = api.get_total_loans_all_time()
    payments = [[x[0], x[1]] for x in api.get_total_payments().items()]
    loans = [[x[0], x[1]] for x in api.get_total_loans_by_month().items()]

    regression = py.Polynomial.fit(
        [x[0] for x in payments], [x[1] for x in payments], 1
    )
    m = payments[-1][0]  # last month

    plt.cld()
    # main graph
    plt.plot([x[0] for x in payments], [x[1] for x in payments], marker="braille")
    # extra regression bits
    plt.plot(
        [m, m + 1, m + 2, m + 3],
        [payments[m - 1][1], regression(m + 1), regression(m + 2), regression(m + 3)],
        marker="braille",
    )
    # plt.ticks_color("red")
    # fix x labels

    xticks = [x[0] for x in payments]
    xticks.extend([m + 1, m + 2, m + 3])

    plt.xticks(xticks, xticks)
    # fix y lables
    plt.yticks([x[1] for x in payments], [format(x[1], ".2f") for x in payments])
    payment_graph = plt.build()

    plt.cld()
    plt.plot([x[0] for x in loans], [x[1] for x in loans], marker="braille")
    plt.xticks([x[0] for x in loans], [x[0] for x in loans])
    plt.yticks([x[1] for x in loans], [format(x[1], ".2f") for x in loans])
    loan_graph = plt.build()

    manager.add(
        ptg.Window(
            "[app.title]Hi Administrator",
            "",
            ptg.Container(
                f"Total amount loaned out: {total_pay}",
                "",
                ptg.Splitter(
                    ptg.Container(
                        "Repayment history:",
                        tabulate.tabulate(
                            payments,
                            headers=["Month", "Paid in"],
                            floatfmt=(".2f"),
                            tablefmt="rounded_grid",
                        ),
                        "",
                        payment_graph,
                        # ptg.Button("Show graph"),
                    ),
                    ptg.Container(
                        "Loan history:",
                        tabulate.tabulate(
                            loans,
                            headers=["Month", "Loaned out"],
                            floatfmt=(".2f"),
                            tablefmt="rounded_grid",
                        ),
                        "",
                        loan_graph,
                    ),
                ),
                static_width=100,
            ),
            overflow=ptg.Overflow.SCROLL,
        ),
        assign="body",
    )


def _login_user(manager: ptg.WindowManager) -> None:
    """Show a list of users and offer the option to register."""

    user_button_list = ptg.Container(static_width=50)
    users = api.get_user_names()

    for u in users:
        user_button_list.__add__(
            ptg.Button(u, lambda x: _show_user_dash(manager, x.label))
        )
        user_button_list.__add__("")

    if len(users) > 0:
        user_button_list.__add__("")

    name = ptg.InputField()
    loan = ptg.InputField()
    rate = ptg.InputField()
    compound = ptg.Checkbox()

    user_button_list.__add__(
        ptg.Collapsible(
            "Not on the list?",
            "",
            "[italic]Let's register!",
            "",
            ptg.Splitter("Name?", name),
            ptg.Splitter("Loan amount?", loan),
            ptg.Splitter("(Decimal) Rate?", rate),
            ptg.Splitter("Compound?", compound),
            "",
            ptg.Button(
                "Submit",
                lambda _: (
                    api.add_user(
                        name.value,
                        float(loan.value),
                        compound.checked,
                        float(rate.value),
                    ),
                    _login_user(manager),
                ),
            ),
        )
    )

    modal = ptg.Window(
        "[app.title]Name?",
        "",
        user_button_list,
        overflow=ptg.Overflow.SCROLL,
    ).center()

    manager.add(modal, assign="body")


def _show_user_dash(manager: ptg.WindowManager, name: str) -> None:
    user_info = api.get_user_details(name)
    data = [[x[0], x[1][0], x[1][1]] for x in api.get_user_payments(name).items()]
    pay_field = ptg.InputField(prompt="How much? ")

    plt.cld()
    plt.plot([x[0] for x in data], [x[2] for x in data], marker="braille")
    plt.xticks([x[0] for x in data], [x[0] for x in data])
    plt.yticks([x[2] for x in data], [format(x[2], ".2f") for x in data])
    pay_graph = plt.build()

    manager.add(
        ptg.Window(
            f"[app.title]Hi {name}",
            "",
            ptg.Container(
                f"You owe {user_info.balance} out of a loan",
                f"of {user_info.principal} at {user_info.rate * 100}% interest pm.",
                "",
                ptg.Splitter(
                    pay_field,
                    ptg.Button(
                        "Pay",
                        lambda _: (
                            (api.make_payment(name, float(pay_field.value))),
                            (_show_user_dash(manager, name)),
                        ),
                    ),
                ),
                "",
                "Previous payments:",
                tabulate.tabulate(
                    data,
                    headers=["Month", "Paid", "Balance"],
                    floatfmt=(".2f"),
                    tablefmt="rounded_grid",
                ),
                "",
                pay_graph,
                static_width=50,
            ),
            overflow=ptg.Overflow.SCROLL,
        ),
        assign="body",
    )


def _confirm_quit(manager: ptg.WindowManager) -> None:
    """Creates an "Are you sure you want to quit" modal window"""

    modal = ptg.Window(
        "[app.title]Are you sure you want to quit?",
        "",
        ptg.Container(
            ptg.Splitter(
                ptg.Button("Yes", lambda _: manager.stop()),
                ptg.Button("No", lambda _: modal.close()),
            ),
            static_width=50,
        ),
    ).center()

    modal.select(1)
    manager.add(modal)


def _update_header(manager: ptg.WindowManager) -> None:
    """Updates the header with the current month."""

    header = ptg.Window(
        f"[app.header] loan repayment app: month {api.get_current_month()} ",
        box="EMPTY",
        is_persistant=True,
    )

    header.styles.fill = "app.header.fill"

    manager.add(header, assign="header")


def main() -> int:
    """Runs the application."""

    _create_aliases()
    _configure_widgets()

    api.init()

    # plt.title("Graph")
    plt.xlabel("month")
    plt.ylabel("$")
    plt.theme("pro")
    plt.plot_size(40, 20)

    with ptg.WindowManager() as manager:
        manager.layout = _define_layout()

        header = ptg.Window(
            f"[app.header] loan repayment app: month {api.get_current_month()} ",
            box="EMPTY",
            is_persistant=True,
        )

        header.styles.fill = "app.header.fill"

        # Since header is the first defined slot, this will assign to the correct place
        manager.add(header)

        footer = ptg.Window(
            ptg.Splitter(
                ptg.Button(
                    "Advance >>", lambda _: (api.tick(), _update_header(manager))
                ),
                ptg.Button("Home", lambda _: _ask_admin(manager)),
                ptg.Button("Quit", lambda _: _confirm_quit(manager)),
            ),
            box="EMPTY",
            overflow=ptg.Overflow.SCROLL,
        )
        footer.styles.fill = "app.footer"

        manager.add(footer, assign="footer")

        _ask_admin(manager)

    api.die()

    return 0
