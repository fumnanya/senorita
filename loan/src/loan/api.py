from peewee import (
    SqliteDatabase,
    Model,
    CharField,
    IntegerField,
    FloatField,
    ForeignKeyField,
    BooleanField,
)

# database setup
db = SqliteDatabase(
    "loan.db",
    pragmas={
        "journal_mode": "wal",
        "cache_size": -1 * 64000,
        "foreign_keys": 1,
        "ignore_check_constraints": 0,
    },
)


class Base(Model):
    class Meta:
        database = db


class Admin(Base):
    """Manages the global time."""

    name = CharField(unique=True)
    month = IntegerField(default=1)  # what month are we in?


class User(Base):
    """Takes loans."""

    name = CharField(unique=True)
    principal = FloatField()  # how much they took
    is_compound = BooleanField()  # is it simple or compound interest
    balance = FloatField()  # how much left
    rate = FloatField()  # interest rate per month
    initial_month = IntegerField()  # when'd they start


class Payment(Base):
    """Tracks payments."""

    user = ForeignKeyField(User, backref="user")
    month = IntegerField()
    paid = FloatField()  # how much they paid
    balance = FloatField()  # how much left


def init():
    db.connect()
    db.create_tables([User, Admin, Payment])

    Admin.get_or_create(name="fum")


def die():
    db.close()


def tick() -> int:
    """Increase the global month count, apply interest to everyone, and return the current month."""
    me: Admin = Admin.get()
    me.month += 1
    me.save()

    for u in User.select():
        if u.is_compound:
            u.balance *= 1 + u.rate  # add interest on total
        else:
            u.balance += u.principal * u.rate  # add interest only on loan

        u.save()

    return me.month


def get_current_month() -> int:
    """Get the current month."""

    return Admin.get().month


# todo: test for dup. users
def add_user(
    name: str,
    principal: float,
    is_compound: bool,
    rate: float,
) -> None:
    """Add a new user, assumes `name` is unique."""
    month: int = Admin.get().month
    balance = principal * (1 + rate)  # add 1st interest

    User(
        name=name,
        principal=principal,
        rate=rate,
        is_compound=is_compound,
        initial_month=month,
        balance=balance,
    ).save()

    return None


def get_user_names() -> list[str]:
    """Get all the users' names."""

    return [u.name for u in User.select()]


def get_user_details(name: str) -> User | None:
    """Get a user's details."""

    return User.get_or_none(User.name == name)


def get_user_payments(name: str) -> dict[int, tuple[float, float]]:
    """Returns a user's payments in the form `{ month: (paid, balance) }`."""

    res = {}

    for p in (
        Payment.select(Payment.month, Payment.paid, Payment.balance)
        .join(User)
        .where(User.name == name)
    ):
        res[p.month] = (p.paid, p.balance)

    return res


def make_payment(name: str, pay: float) -> float:
    """Make a payment in the database. If a month is already present, add to it. Otherwise, create a new one. Returns the amount paid."""

    user: User = User.get(User.name == name)
    month: int = Admin.get().month

    if pay > user.balance:  # don't overpay if there's less left
        pay = user.balance

    user.balance -= pay  # subtract from balance

    user.save()

    # check if there's a payment for this month
    potential = (
        Payment.select()
        .join(User)
        .where((User.name == name) & (Payment.month == month))
        .first()
    )

    if potential is None:
        Payment(user=user, month=month, paid=pay, balance=user.balance).save()
    else:
        potential.paid += pay
        potential.balance = user.balance
        potential.save()

    return pay


def get_total_loans_all_time():
    """Get the total amount loaned out."""

    return sum([u.principal for u in User.select()])


def get_total_loans_by_month() -> dict[int, float]:
    """Gets loans given out by month."""

    res = {}

    for u in User.select(User.initial_month, User.principal):
        if u.initial_month in res:
            res[u.initial_month] += u.principal
        else:
            res[u.initial_month] = u.principal

    return res


def get_total_payments() -> dict[int, float]:
    """Returns the aggregated payments from all loans per month."""

    res = {}

    for p in Payment.select(Payment.month, Payment.paid):
        if p.month in res:
            res[p.month] += p.paid
        else:
            res[p.month] = p.paid

    return res
