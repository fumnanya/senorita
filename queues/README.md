# queues

modelling assignment to simulate queues.

check out the [models](src/queues/models.py) or the [tests](src/queues/test_queue.py).

### to run:

1. install [`rye`](https://rye-up.com/guide/installation/).
2. clone the project folder[^1]: `git clone https://codeberg.org/fumnanya/senorita`.
3. change into this directory: `cd queues`
4. install the dependencies: `rye sync`.
5. run the app: `rye run queues`.

### images:

| |
|:-------:| 
|![](images/run.png)|
| _the simulation running_ |
|![](images/test.png)|
| _test output_ |

[^1]: _alternatively, you can sparse checkout a folder if you don't want the full thing: https://stackoverflow.com/a/52269934/21733670_
