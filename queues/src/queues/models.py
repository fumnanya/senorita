from enum import Enum
from fractions import Fraction
from math import inf


class DisciplineType(Enum):
    FCFS = 0
    LIFO = 1


class SingleServerQueue:
    λ: int  # arrival rate
    μ: int  # service rate
    c: int  # number of servers
    n: int | float  # queue length, can be infinite

    discipline: DisciplineType = DisciplineType.FCFS

    ρ: Fraction  # ratio of arrival to service
    ls: Fraction  # length of system
    lq: Fraction  # length of queue
    ws: Fraction  # waiting time of system
    qs: Fraction  # waiting time of queue

    λeff: Fraction | None  # effective arrival rate, only valid when `n` == `inf`

    def __init__(
        self, arrival_rate: int, service_rate: int, queue_length: int | float
    ) -> None:
        self.λ = arrival_rate
        self.μ = service_rate
        self.c = 1
        self.n = queue_length

        self.ρ = Fraction(self.λ, self.μ)

        if self.n == inf:  # single server, infinite length
            self.ls = self.ρ / (1 - self.ρ)
            self.lq = self.ls - self.ρ
            self.ws = self.ls / self.λ
            self.wq = self.lq / self.λ
            self.λeff = None
        else:  # single server, finite length
            self.ls = (
                self.ρ
                * (
                    1
                    + (self.n * pow(self.ρ, self.n + 1))
                    - ((self.n + 1) * pow(self.ρ, self.n))
                )
            ) / ((1 - self.ρ) * (1 - pow(self.ρ, self.n + 1)))
            self.λeff = self.λ * (1 - self.exactly_n_prob(int(self.n)))
            self.lq = self.ls - (self.λeff / self.μ)
            self.ws = self.ls / self.λeff
            self.wq = self.lq / self.λeff

    def __str__(self) -> str:
        """Returns the queue in Kendall's notation."""

        res = f"M/M/{self.c}/{"∞" if self._is_infinite() else self.n}/∞/{"FCFS" if self.discipline == DisciplineType.FCFS else "LIFO"}\n\n"
        res += f"λ = {self.λ}\n"
        res += f"λeff = {self.λeff}\n" if not self._is_infinite() else ""
        res += f"μ = {self.μ}\n"
        res += f"ρ = {self.ρ}\n"
        res += f"ls = {float(self.ls)}\n"
        res += f"lq = {float(self.lq)}\n"
        res += f"ws = {float(self.ws)}\n"
        res += f"wq = {float(self.wq)}\n"

        return res

    def _is_infinite(self):
        return self.n == inf

    def exactly_n_prob(self, n: int) -> Fraction:
        """Returns the probability that there's `n` people in the system."""

        return (
            pow(self.ρ, n) * (1 - self.ρ)
            if self._is_infinite()
            else pow(self.ρ, n) * ((1 - self.ρ) / (1 - pow(self.ρ, self.n + 1)))
        )

    def at_least_n_prob(self, n: int) -> Fraction:
        """Returns the probability that there's at least `n` people in the system."""

        return 1 - Fraction(sum(self.exactly_n_prob(x) for x in range(0, n)))

    def balk_prob(self) -> Fraction:
        """
        Returns the probability that someone comes into the system but doesn't join it.
        ONLY works for finite-length queues.
        """

        return Fraction(0) if self._is_infinite() else self.exactly_n_prob(int(self.n))

    def server_busy_prob(self) -> Fraction:
        """Returns the probability that the server is busy i.e. at least 1 person is present."""

        return self.at_least_n_prob(1)

    def no_queue_prob(self) -> Fraction:
        """Returns the probability that there's no queue i.e. there's either 0 or 1 people present."""

        return self.exactly_n_prob(0) + self.exactly_n_prob(1)

    def system_expected_people(self) -> Fraction:
        """Returns the expected number of people in the system."""

        return self.ls

    def queue_expected_people(self) -> Fraction:
        """Returns the expected number of people in the queue."""

        return self.lq

    def system_expected_wait(self) -> Fraction:
        """Returns the expected waiting time of the system."""

        return self.ws

    def queue_expected_wait(self) -> Fraction:
        """Returns the expected waiting time of the queue."""

        return self.wq
