from rich import print, prompt
from math import inf
from queues.models import SingleServerQueue


def main() -> int:
    print("[bold yellow]Welcome to your single-server queue simulation!")
    print("[bold]Enter queue details:\n")

    arrival = prompt.IntPrompt.ask("[bold green]Arrival rate? ")
    service = prompt.IntPrompt.ask("[bold green]Service rate? ")

    if prompt.Confirm.ask("[bold green]Infinite?"):
        len = inf
    else:
        len = prompt.IntPrompt.ask("[bold green]Length? ")

    queue = SingleServerQueue(arrival, service, len)

    print(f"\nQueue details:\n{queue}")

    print("[bold cyan]Show me...")
    print("1. Probability of exactly n people")
    print("2. Probability of at least n people")
    print("3. Balking probability")
    print("4. Probability server is busy")
    print("5. Probability of no queue")
    print("6. Expected number of people in the system")
    print("7. Expected number of people in the queue")
    print("8. Expected waiting time for the system")
    print("9. Expected waiting time for in the queue")
    print("--------")
    print("0. Exit")
    print()

    while True:
        res = None

        match prompt.IntPrompt.ask(">", choices=[str(i) for i in range(0, 10)]):
            case 0:
                break

            case 1:
                n = prompt.IntPrompt.ask("n?")
                res = queue.exactly_n_prob(n)

            case 2:
                n = prompt.IntPrompt.ask("n?")
                res = queue.at_least_n_prob(n)

            case 3:
                res = queue.balk_prob()

            case 4:
                res = queue.server_busy_prob()

            case 5:
                res = queue.no_queue_prob()

            case 6:
                res = queue.system_expected_people()

            case 7:
                res = queue.queue_expected_people()

            case 8:
                res = queue.system_expected_wait()

            case 9:
                res = queue.queue_expected_wait()

            case _:
                return -1

        print(f"{res}")
        if not res.is_integer():
            print(f"approx. {float(res)}")
        print()

    return 0
