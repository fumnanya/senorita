from queues.models import SingleServerQueue
from math import isclose, inf
from fractions import Fraction


class TestSingleInfinite:
    q = SingleServerQueue(8, 9, inf)

    def test_init(self):
        assert self.q.ρ == Fraction(8, 9)

    def test_exact_probability(self):
        assert self.q.exactly_n_prob(0) == Fraction(1, 9)
        assert isclose(self.q.exactly_n_prob(10), 0.0341, abs_tol=10**-3)

    def test_at_least_probablity(self):
        assert self.q.at_least_n_prob(1) == Fraction(8, 9)
        assert isclose(self.q.at_least_n_prob(2), 0.7902, abs_tol=10**-4)

    def test_server_busy_probability(self):
        assert self.q.server_busy_prob() == Fraction(8, 9)

    def test_no_queue_probability(self):
        assert isclose(self.q.no_queue_prob(), 0.2098, abs_tol=10**-4)

    def test_expected_wait_times(self):
        assert self.q.system_expected_wait() == 1
        assert isclose(self.q.queue_expected_wait(), 0.888, abs_tol=10**-3)

    def test_expected_people(self):
        assert self.q.system_expected_people() == 8
        assert isclose(self.q.queue_expected_people(), 7.111, abs_tol=10**-3)


class TestSingleFinite:
    q = SingleServerQueue(8, 9, 10)

    def test_init(self):
        assert self.q.ρ == Fraction(8, 9)
        assert isclose(self.q.ls, 3.854, abs_tol=10**-3)

    def test_exact_probability(self):
        assert isclose(self.q.exactly_n_prob(0), 0.1529, abs_tol=10**-4)
        assert isclose(self.q.exactly_n_prob(10), 0.0471, abs_tol=10**-3)

    def test_no_queue(self):
        assert isclose(self.q.no_queue_prob(), 0.2889, abs_tol=10**-4)

    def test_balk_probability(self):
        assert isclose(self.q.balk_prob(), 0.0471, abs_tol=10**-4)

    def test_expected_wait_times(self):
        assert isclose(self.q.queue_expected_wait() * 60, 23.66, abs_tol=10**-2)
        assert isclose(self.q.system_expected_wait(), 0.5055, abs_tol=10**-4)
