# import randoms.models2 as models2

# assignments
#
# track football matches
# 8 teams meet head to head
# generate scores, it should not be more than 8 on a side, use linear
# keep a leaderboard
# go for 5 weeks
#
# generate recharge cards
# use middle square
# generate each of the four tuples with separate seeds
# concatenate

from rich import print
from rich.prompt import IntPrompt

from randoms.football import FootballSimulator
from randoms.recharge import RechargeFactory


def main() -> int:
    print("[bold cyan]Welcome to the app!")
    print()
    print("1. Football matches")
    print("2. Recharge cards")
    print()

    match IntPrompt.ask("What do you want to do?", choices=["1", "2"]):
        case 1:
            print("[bold cyan]***** Football Sim *****")
            print()
            print("Input eight team names seperated by spaces:")
            names = input().split()

            try:
                sim = FootballSimulator(names)
            except ValueError as e:
                print(f"[red]{e}")
                return 1

            print()
            for i in range(1, 9):
                print(f"[bold green]----- Matchweek {i} -----\n")
                sim.matchweek()
                print()
                print("1. Next")
                print("2. Standings")
                print()

                match IntPrompt.ask(">", choices=["1", "2"]):
                    case 1:
                        pass
                    case 2:
                        sim.standings()
                        input()
                    case _:
                        print("[red]something bad happened.")
                        return 1
                print()
            print("[bold green]Simulation complete!")
        case 2:
            print("[bold cyan] ***** Recharge card factory *****")
            print()
            print("Enter four seeds separated by spaces:")
            seeds = input().split()
            print()

            try:
                factory = RechargeFactory(seeds)
            except ValueError as e:
                print(f"[red]{e}")
                return 1

            for i in range(
                IntPrompt.ask("How many recharge cards need to be generated?")
            ):
                print(f'"{factory.generate_card()}"')

            print("[bold green]Simulation complete!")

        case _:
            print("[red]something bad happened.")
            return 1

    return 0
