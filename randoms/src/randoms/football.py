from itertools import batched
from time import time

from rich import print
from tabulate import tabulate

from randoms.models import LinearCongruentGenerator as lcg


class FootballSimulator:
    """Manages teams and standings"""

    __teams: dict[str, int]  # map teams to table points
    __random: lcg
    MAX_SCORE = 5

    def __init__(self, team_names: list[str]) -> None:
        if len(set(team_names)) != 8:
            raise ValueError("You need to provide eight unique teams")

        self.__teams = {}

        for t in team_names:
            self.__teams[t] = 0

        # use the current time as a source of entropy
        self.__random = lcg(int(time()), 1664525, 1013904223, 2**32)

    def match(self, a: str, b: str) -> tuple[int, int]:
        """Generate scores for a match between `a` and `b`, and update the table"""
        keys = self.__teams.keys()
        if a not in keys or b not in keys:
            raise ValueError(f"{a} or {b} not in lst of teams")

        a_score = self.__random.rand_int() % self.MAX_SCORE + 1
        b_score = self.__random.rand_int() % self.MAX_SCORE + 1

        if a_score > b_score:
            self.__teams[a] += 3
        elif b_score > a_score:
            self.__teams[b] += 3
        else:
            self.__teams[a] += 1
            self.__teams[b] += 1

        return (a_score, b_score)

    def gen_matchups(self) -> list[tuple[str, str]]:
        """Randomly pick teams to play each other."""

        copy = list(self.__teams.keys())
        res: list[str] = []

        while (copy_len := len(copy)) != 0:
            idx = self.__random.rand_int() % copy_len
            res.append(copy.pop(idx))

        return list(batched(res, 2))

    def matchweek(self) -> None:
        """Pretty-print the match scores."""

        matchups = self.gen_matchups()

        for m in matchups:
            a, b = self.match(m[0], m[1])
            print(f"{m[0]} vs {m[1]}: [bold cyan]{a}-{b}[/bold cyan]")

    def standings(self) -> None:
        """Pretty-print the table."""

        print(
            tabulate(
                sorted(self.__teams.items(), key=lambda x: x[1], reverse=True),
                tablefmt="fancy_outline",
                headers=["Team", "Points"],
            )
        )
