from time import time

from randoms.models import MiddleSquareGenerator as msg


class RechargeFactory:
    """Manages generating recharge cards."""

    __random: msg
    __streams: list[list[str]]  # iterators of random numbers
    STREAM_COUNT = 200

    def __init__(self, seeds: list[str]) -> None:
        self.__random = msg(str(int(x if (x := time()) % 2 == 0 else x + 1)))
        self.__streams = []

        if len(seeds) != 4:
            raise ValueError("needs 4 seeds")

        for x in seeds:
            if len(x) != 4:
                raise ValueError("all inputs must be 4 digits long")

            # generate 100 numbers with this seed
            gen = msg(x)
            self.__streams.append(
                [str(gen.rand_int()).zfill(4) for _ in range(self.STREAM_COUNT)]
            )

    def generate_card(self) -> str:
        res = ""

        for x in self.__streams:
            # get a random position
            res += x[self.__random.rand_int() % self.STREAM_COUNT]
            res += "-"

        return res.rstrip("-")
