class LinearCongruentGenerator:
    """This is a pseudo-random number generator that implements the [LCG algorithm](https://en.wikipedia.org/wiki/Linear_congruential_generator)."""

    __first: int
    __seed: int  # x0
    __mult: int  # a
    __inc: int  # c
    __mod: int  # m

    def __init__(
        self,
        x0: int,
        a: int,
        c: int,
        m: int,
    ) -> None:
        self.__seed = self.__first = x0
        self.__mult = a
        self.__inc = c
        self.__mod = m

    def __iter__(self):
        return self

    def __next__(self) -> int:
        """Advance the iterator by applying `(a * x + c) % m`."""

        self.__seed = (self.__mult * self.__seed + self.__inc) % self.__mod
        return self.__seed

    def rand_int(self) -> int:
        """Returns a random number between 0 and `self.__mod`."""

        return self.__next__()

    def random(self) -> float:
        """Returns a random number between 0 and 1."""

        return self.__next__() / self.__mod

    def period(self) -> int:
        """Returns the period of the iterator."""

        count = 0

        for i in LinearCongruentGenerator(
            self.__first, self.__mult, self.__inc, self.__mod
        ):
            count += 1
            if i == self.__first:
                break

        return count


class MiddleSquareGenerator:
    """This is a pseudo-random number generator that implements the [Middle-square method algorithm](https://en.wikipedia.org/wiki/Middle-square_method)."""

    __seed: str
    __first: str
    __found: set[int]

    def __init__(self, n: str) -> None:
        if len(n) % 2 == 1:
            raise ValueError("Seed length must be even")

        try:
            int(n)
        except ValueError:
            raise ValueError("Seed must be a number")

        self.__seed = self.__first = n
        self.__found = set()

    def __iter__(self):
        return self

    def __next__(self) -> int:
        """Advance the iterator by squaring the number and getting the middle digits."""

        seed_len = len(self.__seed)

        # multiply, pad, get middle numbers
        self.__seed = str(int(self.__seed) * int(self.__seed)).zfill(2 * seed_len)[
            seed_len // 2 : -seed_len // 2
        ]

        self.__found.add(int(self.__seed))

        return int(self.__seed)

    def __in_found(self, n: int) -> bool:
        """Checks whether this number been generated before."""

        return n in self.__found

    def rand_int(self) -> int:
        """Returns a random number."""

        return self.__next__()

    def period(self) -> int:
        """Returns the period of the iterator."""

        new = MiddleSquareGenerator(self.__first)
        count = 0

        for i in new:
            count += 1
            if new.__in_found(i):
                break

        return count
