from pytest import raises

from randoms.models import MiddleSquareGenerator as msg


def test_init():
    assert msg("675248").rand_int() == 959861


def test_period():
    assert msg("00").period() == 1
    assert msg("0540").period() == 4


def test_invalid():
    with raises(ValueError):
        msg("0")
        msg("not a num")
