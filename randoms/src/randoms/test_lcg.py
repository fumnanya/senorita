from randoms.models import LinearCongruentGenerator as lcg


def test_init():
    assert [i for i, _ in zip(lcg(27, 17, 43, 100), range(3))] == [2, 77, 52]


def test_period():
    assert lcg(1, 13, 0, 64).period() == 16
    assert lcg(2, 13, 0, 64).period() == 8
    assert lcg(3, 13, 0, 64).period() == 16
    assert lcg(4, 13, 0, 64).period() == 4
