# randoms

modelling assignment to demonstrate some pseudo-random number generators.

supports:

- [x] [linear congruential generator](https://en.wikipedia.org/wiki/Linear_congruential_generator)
- [x] [middle square method](https://en.wikipedia.org/wiki/Middle-square_method)

check out the [code](src/randoms/models.py) or [tests](src/randoms/test_lcg.py).


#### to run:

1. install [`rye`](https://rye-up.com/guide/installation/).
2. clone the project folder[^1]: `git clone https://codeberg.org/fumnanya/senorita`.
3. change into this directory: `cd randoms`.
4. install the dependencies: `rye sync`.
5. run the app: `rye run randoms`.

#### to test:

```sh
$ rye run pytest
```

### images:

||
|:-:| 
|![](images/football.png)|
|_football simulator using LCG_ |
|![](images/recharge.png)|
| _recharge card generator using MSM_ |

[^1]: _alternatively, you can sparse checkout a folder if you don't want the full thing: https://stackoverflow.com/a/52269934/21733670_.
