# some assignments/things i did in my final semester

- [encrypt](encrypt/): a little bunch of ciphers - caesar, vignere, vernam, and weird RSA
- [loan](loan/): a model of a loan repayment application
- [queues](queues/): models of some queues (finite/infinite)
- [randoms](randoms/): models of some PSRNGs

check the folders for their READMEs if you wanna see them.
