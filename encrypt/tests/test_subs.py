from encrypt.ciphers import substitution
from encrypt.ciphers.substitution import gen_permutation as gp


def test_permutation():
    assert gp("zebras") == list("ZEBRASCDFGHIJKLMNOPQTUVWXY")


def test_subs_encrypt():
    assert (
        substitution.encrypt("Flee at once. We are discovered!", gp("zebras"))
        == "Siaa zq lkba. Va zoa rfpbluaoar!"
    )


def test_subs_decrypt():
    assert (
        substitution.decrypt("Siaa zq lkba. Va zoa rfpbluaoar!", gp("zebras"))
        == "Flee at once. We are discovered!"
    )
