from encrypt.ciphers import vernam


def test_vernam_encrypt():
    assert vernam.encrypt("ATTACKATDAWN", "LEMON") == "LXFOPVEFRNHR"


def test_vernam_decrypt():
    assert vernam.decrypt("LXFOPVEFRNHR", "LEMON") == "ATTACKATDAWN"


def test_vernam_encrypt_sentence():
    assert vernam.encrypt("Attack At Dawn!", "LEMON") == "Lxfopv Ef Rnhr!"


def test_vernam_decrypt_sentence():
    assert vernam.decrypt("Lxfopv Ef Rnhr!", "LEMON") == "Attack At Dawn!"
