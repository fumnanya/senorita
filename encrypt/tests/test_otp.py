from encrypt.ciphers import otp
from pytest import raises


def test_otp__encrypt_short():
    with raises(ValueError):
        otp.encrypt("ATTACKATDAWN", "LEMON")


def test_otp__decrypt_short():
    with raises(ValueError):
        otp.decrypt("ATTACKATDAWN", "LEMON")


def test_otp_encrypt():
    assert otp.encrypt("Hello!", "XMCKLA") == "Eqnvz!"


def test_otp_decrypt():
    assert otp.decrypt("Eqnvz!", "XMCKLA") == "Hello!"
