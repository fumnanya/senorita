from encrypt.ciphers import caesar


def test_caesar_encrypt():
    assert caesar.encrypt("aaA", 3) == "ddD"


def test_caesar_decrypt():
    assert caesar.decrypt("ddD", 3) == "aaA"


def test_caesar_encrypt_overflow():
    assert caesar.encrypt("zZ z", 3) == "cC c"


def test_caesar_decrypt_overflow():
    assert caesar.decrypt("cC c", 3) == "zZ z"


def test_caesar_encrypt_sentence():
    assert (
        caesar.encrypt("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG!!", 23)
        == "QEB NRFZH YOLTK CLU GRJMP LSBO QEB IXWV ALD!!"
    )


def test_caesar_decrypt_sentence():
    assert (
        caesar.decrypt("QEB NRFZH YOLTK CLU GRJMP LSBO QEB IXWV ALD!!", 23)
        == "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG!!"
    )
