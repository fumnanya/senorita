from encrypt.ciphers.rsa import (
    is_prime,
    encode_private_key,
    encode_public_key,
    decode_private_key,
    decode_public_key,
    gen_keypair,
    encrypt,
    decrypt,
)


def test_primegen():
    assert not is_prime(1)
    assert is_prime(2)
    assert is_prime(3)
    assert not is_prime(4)
    assert is_prime(17)
    assert not is_prime(1000000)


def test_public_encoding():
    a, b = 3233, 17

    assert decode_public_key(encode_public_key(3233, 17)) == (a, b)


def test_private_encoding():
    p, q, d = 61, 53, 413

    assert decode_private_key(encode_private_key(p, q, d)) == (p, q, 53, 49, 38)


def test_rsa():
    pub, priv = gen_keypair()
    cipher = encrypt("hiiiiiii", pub)

    assert (decrypt(cipher, priv)) == "hiiiiiii"
