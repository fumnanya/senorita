# app.py
from flask import Flask, request, render_template
from encrypt.ciphers import caesar, otp, vernam

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def index():
    result = None

    if request.method == "POST":
        encryption_type = request.form.get("encryption")
        text = request.form.get("text")
        file = request.files.get("file")
        key = request.form.get("key")
        shift = request.form.get("shift")

        if file and file.filename:
            text = file.read().decode("utf-8")

        if text:
            if encryption_type == "caesar":
                if not shift:
                    result = "Error: Shift is required for Caesar encryption"
                else:
                    result = caesar.encrypt(text, int(shift))
            elif encryption_type == "otp":
                if not key:
                    result = "Error: Key is required for OTP encryption"
                else:
                    result = otp.encrypt(text, key)
                result = otp.encrypt(text, "zerba")
            elif encryption_type == "vernam":
                if not key:
                    result = "Error: Key is required for Vernam encryption"
                else:
                    result = vernam.encrypt(text, key)

    return render_template("index.html", result=result)


if __name__ == "__main__":
    app.run(debug=True)
