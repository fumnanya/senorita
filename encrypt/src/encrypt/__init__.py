import time

from rich import print
from rich.progress import track
from rich.prompt import Confirm, IntPrompt, Prompt

from encrypt.ciphers import caesar, otp, rsa, substitution, vernam


def main() -> int:
    try:
        print("[bold]Welcome to the application![/bold]")  # main menu
        print("1. Caesar")  # list of ciphers
        print("2. Substitution")
        print("3. Vernam-Vigenère")
        print("4. One-time pad")
        print("5. RSA")
        print()

        # which are we using?
        cipher = IntPrompt.ask(
            "[bold blue]Choose a cipher[/bold blue]",
            choices=["1", "2", "3", "4", "5"],
        )

        # what are we doing?
        op = Prompt.ask(
            "[bold green]Encrypting or decrypting?[/bold green]", choices=["e", "d"]
        )
        print()

        # text or file?
        is_file = False
        path = ""
        match Confirm.ask(
            f"[bold red]Do you have a file to {'encrypt' if op == 'e' else 'decrypt'}?[/bold red]"
        ):
            case True:
                path = Prompt.ask("[bold red]Path to file?[/bold red]")

                try:
                    with open(path, "r") as f:
                        phrase = f.read()
                        is_file = True
                except FileNotFoundError:
                    print("[red]File not found.[/red]")
                    return 1

            case False:
                phrase = Prompt.ask(
                    f"[bold red]Phrase to {'encrypt' if op == 'e' else 'decrypt'}?[/bold red]"
                )

        match cipher:
            case 1:  # caesar
                # shift by?
                n = IntPrompt.ask(
                    f"[bold red]{'Right' if op == 'e' else 'Left'} shift?[/bold red]"
                )
                res = (
                    caesar.encrypt(phrase, n)
                    if op == "e"
                    else caesar.decrypt(phrase, n)
                )

            case 2:  # subs
                key = Prompt.ask("[bold red]Key?[/bold red]")

                try:
                    perm = substitution.gen_permutation(key)
                except ValueError as e:
                    print(f"[red]{e}[/red]")
                    return 1
                else:
                    match Confirm.ask(
                        f"Is the permutation correct: [blue]{''.join(perm)}[/blue]?"
                    ):
                        case False:
                            print("[red]Please try again.[/red]")
                            return 1
                        case True:
                            res = (
                                substitution.encrypt(
                                    phrase, substitution.gen_permutation(key)
                                )
                                if op == "e"
                                else substitution.decrypt(
                                    phrase, substitution.gen_permutation(key)
                                )
                            )

            case 3:  # vernam
                key = Prompt.ask("[bold red]Key?[/bold red]")

                res = (
                    vernam.encrypt(phrase, key)
                    if op == "e"
                    else vernam.decrypt(phrase, key)
                )

            case 4:  # otp
                key = Prompt.ask("[bold red]Key?[/bold red]")

                try:
                    res = (
                        otp.encrypt(phrase, key)
                        if op == "e"
                        else otp.decrypt(phrase, key)
                    )
                except ValueError as e:
                    print(f"[red]{e}[/red]")
                    return 1

            case 5:
                if op == "e":
                    match Confirm.ask("[bold red]Do you have a public key?[/bold red]"):
                        case True:
                            pub = Prompt.ask("[bold red]Key?[/bold red]")

                        case False:
                            print()
                            print(
                                "[bold green]Generating public-private key pair...[/bold green]"
                            )
                            pub, priv = rsa.gen_keypair()

                            print(f'[bold]Public: [/bold]"{pub}"')
                            print(f'[bold]Private: [/bold]"{priv}"')

                            print("[bold red]DO NOT SHARE YOUR PRIVATE KEY![/bold red]")

                    res = rsa.encrypt(phrase, pub)
                else:
                    priv = Prompt.ask("[bold red]Private key?[/bold red]")
                    res = rsa.decrypt(phrase, priv)

            case _:
                print("[red]Something went wrong.[/red]")
                return 1

        print()
        for _ in track(range(10), description="Processing..."):
            time.sleep(0.05)  # fake work
        print()
        if is_file:
            with open(f"{'encrypted-' if op == 'e' else 'decrypted-'}{path}", "w") as f:
                f.write(res)
                print(
                    f"[bold]File written to {'encrypted-' if op == 'e' else 'decrypted-'}{path}[/bold]"
                )
        else:
            print(f'Result: "{res}"')

        return 0
    except KeyboardInterrupt:
        print("\n[red]Caught Ctrl+C, Exiting...[/red]")
        return 1
