# this is more vanilla Vigenère than Vernam-Vigenère
# Vernam invented XOR to combine the key & letter bits
# https://patents.google.com/patent/US1310719
# https://en.wikipedia.org/wiki/Gilbert_Vernam

from itertools import cycle


def encrypt(text: str, key: str) -> str:
    """Gets the numeric values of every letter in `text` and add it with the associated `key` value."""
    res = ""
    keys = cycle(key.upper())  # so we can skip spaces

    for c in text:
        if c.isalpha():
            s = 65 if c.isupper() else 97
            res += chr(s + (ord(c) - s + ord(next(keys)) - 65) % 26)
        else:
            res += c

    return res


def decrypt(text: str, key: str) -> str:
    """Gets the numeric values of every letter in `text` and subtract the associated `key` value from it."""
    res = ""
    keys = cycle(key.upper())  # so we can skip spaces

    for c in text:
        if c.isalpha():
            s = 65 if c.isupper() else 97
            res += chr(s + (ord(c) - s - ord(next(keys)) - 65) % 26)
        else:
            res += c

    return res
