import random
from math import ceil, sqrt, lcm
from base64 import b64encode, b64decode


def is_prime(x: int):
    if x == 1:
        return False

    if x == 2:
        return True

    for i in range(2, ceil(sqrt(x)) + 1):
        if x % i == 0:
            return False

    return True


def gen_keypair() -> tuple[str, str]:
    while True:
        if is_prime((p := random.randrange(1000000, 100000000000000))):
            break

    while True:
        if is_prime((q := random.randrange(1000000, 100000000000000))):
            break

    n = p * q
    λn = lcm(p - 1, q - 1)
    e = 65537  # e and λn are co-prime

    d = pow(e, -1, λn)  # modular mult. inv., 1/e mod λn

    public = encode_public_key(n, e)
    private = encode_private_key(p, q, d)

    return (public, private)


def encode_public_key(n: int, e: int) -> str:
    """Takes `n` and `e` and returns base64."""

    return b64encode(bytes(str(n) + "," + str(e), "utf-8")).decode()


def decode_public_key(s: str) -> tuple[int, int]:
    """Takes a base64 string and spits out the two comma-sep. numbers inside."""

    nums = [int(x) for x in b64decode(s).decode().split(",")]
    return (nums[0], nums[1])


def encode_private_key(p: int, q: int, d: int) -> str:
    """Takes key params. and spits out a base64 representation of them."""

    dp = d % (p - 1)
    dq = d % (q - 1)
    qi = pow(q, -1, p)

    res = ",".join(str(x) for x in [p, q, dp, dq, qi])

    return b64encode(bytes(res, "utf-8")).decode()


def decode_private_key(s: str) -> tuple[int, int, int, int, int]:
    """Takes a base64 string and spits out the five comma-sep. numbers inside."""

    nums = [int(x) for x in b64decode(s).decode().split(",")]
    return (nums[0], nums[1], nums[2], nums[3], nums[4])


def encrypt(text: str, key: str) -> str:
    """Uses `n` and `e` from `key` to encrypt every byte."""

    n, e = decode_public_key(key)

    res = ",".join([str((b**e) % n) for b in bytes(text, "utf-8")])

    return b64encode(bytes(res, "utf-8")).decode()


def decrypt(text: str, key: str) -> str:
    """Uses `p`, `q`, `dp`, `dq`, `qi` from `key` to decrypt every byte."""

    p, q, dp, dq, qi = decode_private_key(key)

    res = ""

    for b in b64decode(text).decode().split(","):
        # using chinese remainder theorem
        # because `res += chr((int(b) ** d) % n)` is very slow

        b = int(b)
        m1 = pow(b, dp, p)
        m2 = pow(b, dq, q)
        h = (qi * (m1 - m2)) % p
        m = m2 + h * q

        res += chr(m)

    return res
