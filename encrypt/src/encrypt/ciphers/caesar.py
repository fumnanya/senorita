def encrypt(text: str, shift: int) -> str:
    """Shifts every character in `text` right by `shift` characters."""

    return "".join(
        c
        if not c.isalpha()  # skip symbols
        else chr(65 + ((ord(c) - 65 + shift) % 26))  # get offset from A, shift, wrap
        if c.isupper()
        else chr(97 + ((ord(c) - 97 + shift) % 26))  # lowercase
        for c in text
    )


def decrypt(text: str, shift: int) -> str:
    """Shifts every character in `text` left by `shift` characters."""

    return "".join(
        c
        if not c.isalpha()
        else chr(65 + (ord(c) - 65 - shift) % 26)
        if c.isupper()
        else chr(97 + (ord(c) - 97 - shift) % 26)
        for c in text
    )
