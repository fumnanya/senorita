def gen_permutation(key: str) -> list[str]:
    """
    Generates an alphabet permutation from a key, since it might not be long enough.

    Example:
    gen_permutation("zebra") == "ZEBRASCDFGHIJKLMNOPQTUVWXY"
    """

    res = list(key.upper())

    # ensure no duplicates
    if res != list(dict.fromkeys(res)):
        raise ValueError("Key must not contain duplicate characters.")

    res.extend([c for c in "ABCDEFGHIJKLMNOPQRSTUVWXYZ" if c not in key.upper()])

    return res


def encrypt(text: str, perm: list[str]) -> str:
    """Takes a permutation and uses it to swap characters in `text`."""

    return "".join(
        c  # skip symbols
        if not c.isalpha()
        else perm[ord(c) - 65]  # get offset from A and index
        if c.isupper()
        else perm[ord(c) - 97].lower()  # same but for a
        for c in text
    )


def decrypt(text: str, perm: list[str]) -> str:
    """Takes a permutation and uses it to swap characters back in `text`."""

    return "".join(
        c  # skip symbols
        if not c.isalpha()
        else chr(65 + perm.index(c))  # look up offset and convert to char
        if c.isupper()
        else chr(97 + perm.index(c.upper())).lower()
        for c in text
    )
