from itertools import cycle


def encrypt(text: str, key: str) -> str:
    """Gets the numeric values of every letter in `text` and add it with the associated `key` value."""
    res = ""

    if len(key) < len(text):
        raise ValueError("Key must be at least the same length as the text.")

    cipher = cycle(key.upper())  # so we can skip spaces

    for c in text:
        if c.isalpha():
            s = 65 if c.isupper() else 97
            res += chr(s + (ord(c) - s + ord(next(cipher)) - 65) % 26)
        else:
            res += c

    return res


def decrypt(text: str, key: str) -> str:
    """Gets the numeric values of every letter in `text` and subtract the associated `key` value from it."""
    res = ""

    if len(key) < len(text):
        raise ValueError("Key must be at least the same length as the text.")

    cipher = cycle(key.upper())  # so we can skip spaces

    for c in text:
        if c.isalpha():
            s = 65 if c.isupper() else 97
            res += chr(s + (ord(c) - s - ord(next(cipher)) - 65) % 26)
        else:
            res += c

    return res
