# encrypt

to run:

```sh
$ rye sync 
$ rye run encrypt
```

to test:

```sh
$ rye run pytest
```

computer security assignment to demonstrate some encryption techniques.

supported ciphers:

- [X] Caeser
- [X] Substitution
- [X] Vernam-Vigenère [^2]
- [X] One-time Pad
- [X] RSA [^3]

check out the [ciphers](src/encrypt/ciphers/) or [tests](tests/).

(note: the teacher wanted a flask implementation, which I did, but he said "copy and paste your code in .docx files" and ".docx is more portable than .pdf" at which point I mentally checked out...so enjoy those submissions with the code [here](submissions/)).

### to run:

1. install [`rye`](https://rye-up.com/guide/installation/).
2. clone the project folder[^1]: `git clone https://codeberg.org/fumnanya/senorita`.
3. change into this directory: `cd encrypt`.
4. install the dependencies: `rye sync`.
5. run the app: `rye run encrypt` (or `rye run web` for the flask version).

### images:

| |
|:-------:| 
|![](images/decrypting.png)|
| _decrypting_ |
|![](images/encrypting.png)|
| _encrypting_ |
|![](images/web.png)|
| _web version_ |

[^1]: _alternatively, you can sparse checkout a folder if you don't want the full thing: https://stackoverflow.com/a/52269934/21733670_.
[^2]: _the implementation in the notes is more plain Vigenère than Vernam-Vigenère, see [this comment](src/encrypt/ciphers/vernam.py#L1)_.
[^3]: albeit, with non-standand key and ciphertext formats.
